#include <stdio.h>
#include <stdlib.h>

#define N1 3
#define N2 3
#define N 6

// x,y,z의 최대 차수가 각각 nx,ny,nz인
// 다항식에 대한 삼중 포인터를 받아 출력하는 함수로
// 출력방식은 x의 차수에 대한 내림차순 최우선하며
// x의 차수가 같은 항끼리는 y에 대한 내림차순을 우선하고
// 마지막으로 x와 y의 차수가 둘 다 같을 때는 z에 대한
// 내림차순으로 각 항들을 출력한다.
void printpoly3(int nx, int ny, int nz, int*** p);

// x,y,z의 최대 차수 nx1,ny1,nz1 및 nx2,ny2,nz2인
// 두 다항식에 대한 삼중 포인터 p1와 p2의 곱을
// x,y,z의 최대 차수 각각 nx,ny,nz 까지 저장 가능한
// 삼중 포인터 dest에 저장하는 함수이다.
int*** multpoly3(
         int nx,  int ny,  int nz,  int ***dest,
         int nx1, int ny1, int nz1, int ***p1,
         int nx2, int ny2, int nz2, int ***p2);

int main(void)
{
    int poly1[N1][N1][N1];
    int poly2[N2][N2][N2];
    int poly[N][N][N] = {}; // 모두 0으로 초기화

    for (int i=0; i<N1; ++i)
        for (int j=0; j<N1; ++j)
            for (int k=0; k<N1; ++k)
                scanf("%d", &(poly1[i][j][k])); // x^iy^jz^k의 계수 입력

    for (int i=0; i<N2; ++i)
        for (int j=0; j<N2; ++j)
            for (int k=0; k<N2; ++k)
                scanf("%d", *(*(poly2+i)+j)+k); // x^iy^jz^k의 계수 입력

    // 3차원 배열 poly1에 대응되는 3중 포인터 p1를 동적 할당으로 정의
    int ***p1 = (int***)malloc(N1*sizeof(int**));
	for (int i=0; i<N1; i++){
		p1[i] = (int**)malloc(N1*sizeof(int*));
		for (int j=0; j<N1; j++){
			p1[i][j] = (int*)malloc(N1*sizeof(int));
			for(int k=0; k<N1; k++){
                p1[i][j][k] = poly1[i][j][k];
			}
		}
	}
    // p1이 poly1에 대응되는 3중 포인터가 되도록 마무리하라

    // 3차원 배열 poly2에 대응되는 3중 포인터 p2를 동적 할당으로 정의
    int ***p2 = (int***)malloc(N2*sizeof(int**));
	for (int i=0; i<N2; i++){
		p2[i] = (int**)malloc(N2*sizeof(int*));
		for (int j=0; j<N2; j++){
			p2[i][j] = (int*)malloc(N2*sizeof(int));
			for(int k=0; k<N2; k++){
                p2[i][j][k] = poly2[i][j][k];
			}
		}
	}
    // p2가 poly2에 대응되는 3중 포인터가 되도록 마무리하라

    // 3차원 배열 poly에 대응되는 3중 포인터 p를 동적 할당으로 정의
    int ***p = (int***)malloc(N*sizeof(int**));
	for (int i=0; i<N; i++){
		p[i] = (int**)malloc(N*sizeof(int*));
		for (int j=0; j<N; j++){
			p[i][j] = (int*)malloc(N*sizeof(int));
			for(int k=0; k<N; k++){
                p[i][j][k] = poly[i][j][k];
			}
		}
	}
    // p가 poly에 대응되는 3중 포인터가 되도록 마무리하라

    multpoly3(N,N,N,p, N1,N1,N1,p1, N2,N2,N2,p2);

    printf("  "); printpoly3(N1,N1,N1,p1); printf("\n");
    printf("* "); printpoly3(N2,N2,N2,p2); printf("\n");
    printf("------------------------------------------------------------\n");
    printf("  "); printpoly3(N,N,N,p); printf("\n");

        for(int i=0; i<N1; i++){
            for(int j=0; j<N1; j++){
                free(p1[i][j]);
            }
            free(p1[i]);
        }
        free(p1);
        for(int i=0; i<N1; i++){
            for(int j=0; j<N1; j++){
                free(p2[i][j]);
            }
            free(p2[i]);
        }
        free(p2);
        for(int i=0; i<N; i++){
            for(int j=0; j<N; j++){
                free(p[i][j]);
            }
            free(p[i]);
        }
        free(p);

    return 0;
}

void printpoly3(int nx, int ny, int nz, int*** p)
{
	int chk = 0;
    for(int i=0; i<nx; i++){
    	for(int j=0; j<ny; j++){
    		for(int k=0; k<nz; k++){
    			if(p[i][j][k]!=0){
                    if(chk!=0) printf(" ");
					if(p[i][j][k]==1){
						if(chk!=0) printf("+");
						if(i==0 && j==0 && k==0) printf("1");
					}
					else if(p[i][j][k]==-1){
						printf("-");
						if(i==0 && j==0 && k==0) printf("1");
					}
					else if(chk==0) printf("%d", p[i][j][k]);
					else printf("%+d", p[i][j][k]);
	    			if(i!=0) printf("x");
	    			if(i>1) printf("^%d", i);
	    			if(j!=0) printf("y");
	    			if(j>1) printf("^%d", j);
	    			if(k!=0) printf("z");
	    			if(k>1) printf("^%d", k);
					chk++;
				}
			}
		}
	}
	if(chk==0) printf("0");
}

int*** multpoly3(
         int nx,  int ny,  int nz,  int ***dest,
         int nx1, int ny1, int nz1, int ***p1,
         int nx2, int ny2, int nz2, int ***p2)
{
	for (int i=0; i<nx1*ny1*nz1; i++){
		for (int j=0; j<nx2*ny2*nz2; j++){
			int p1x = i/(ny1*nz1);
			int p1y = i/ny1-(i/(ny1*nz1)*ny1);
			int p1z = i%nz1;
			int p2x = j/(ny2*nz2);
			int p2y = j/ny2-(j/(ny2*nz2)*ny2);
			int p2z = j%nz2;
			int destx = p1x+p2x;
			int desty = p1y+p2y;
			int destz = p1z+p2z;
			dest[destx][desty][destz] += p1[p1x][p1y][p1z] * p2[p2x][p2y][p2z];
		}
	}
    return dest;
}
