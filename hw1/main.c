#include <stdio.h>

void triR(void) {
  int size, repeat;
	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");
  
  // ...
  // 이 함수를 완성하시오. (4점)
  // ...
	int r = 0, i = 0, j = 0;
	for(r = 1; r <= repeat; r++) // 반복 설정
	{
			printf("\n");
			for(i = 1; i <= size; i++)
			{
					for(j = 1; j <= i; j++)
					{
						printf("%d", i);
					}
				printf("\n");
			} // 증가 끗
			for(i = size-1; i > 0; i--) // 감소 시작
			{
					for(j = 1; j <= i; j++)
					{
						printf("%d", i);
					}
				printf("\n");
			} // 증가 끗
	}
	printf("Bye world\n");
}

void triL(void) {
  int size, repeat;
	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");
  
  // ...
  // 이 함수를 완성하시오. (1점)
  // ...
	int r = 0, i = 0, j = 0;
	for(r = 1; r <= repeat; r++) // 반복 설정
	{
		for(j = 1; j <= size-i; j++)
		{
			printf(".");
		}
		printf("\n");
			for(i = 1; i <= size; i++)
			{
					for(j = 1; j <= size-i; j++)
					{
						printf(".");
					}
					for(j = 1; j <= i; j++)
					{
						printf("%d", i);
					}
				printf("\n");
			} // 증가 끗
			for(i = size-1; i > 0; i--) // 감소 시작
			{
					for(j = 1; j <= size-i; j++)
					{
						printf(".");
					}
					for(j = 1; j <= i; j++)
					{
						printf("%d", i);
					}
				printf("\n");
			} // 증가 끗
	}
	printf("Bye world\n");
}

void dias(void) {
  int size, repeat;
	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");
  
  // ...
  // 이 함수를 완성하시오. (1점)
  // ...
	int r = 0, i = 0, j = 0;
	for(r = 1; r <= repeat; r++) // 반복 설정
	{
		for(j = 1; j <= size-i; j++)
		{
			printf(".");
		}
		printf("\n");
			for(i = 1; i <= size; i++)
			{
					for(j = 1; j <= size-i; j++)
					{
						printf(".");
					}
					for(j = 1; j <= i*2; j++)
					{
						printf("%d", i);
					}
				printf("\n");
			} // 증가 끗
			for(i = size-1; i > 0; i--) // 감소 시작
			{
					for(j = 1; j <= size-i; j++)
					{
						printf(".");
					}
					for(j = 1; j <= i*2; j++)
					{
						printf("%d", i);
					}
				printf("\n");
			} // 증가 끗
	}
	printf("Bye world\n");
}

int main(void)
{
  int n;
  scanf("%d", &n); // 1,2,3 중 하나를 입력받는다
  switch (n)
  {
    case 1: triR(); break;
    case 2: triL(); break;
    case 3: dias(); break;
    default: return -1;
  }
  return 0;
}
